package com.worldwide;

import com.worldwide.dao.AcrProcessDAO;
import com.worldwide.model.AcrRecord;
import com.worldwide.model.SshRecord;
import com.worldwide.service.AcrProcessService;
import com.worldwide.service.EmailService;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
@Configuration
@ComponentScan(basePackages = { "com.worldwide" })
@EnableScheduling
public class AcrProcessMain {
	static final Logger logger = Logger.getLogger(AcrProcessMain.class);
	@Autowired
	private AcrProcessService acrProcessService;
	@Autowired
	private AcrProcessDAO acrProcessDAO;
	@Autowired
	private EmailService emailService;

	public void processAcr(String recordType) {
		AcrRecord acrRecord = new AcrRecord();
		boolean checkForAnyRecordsToProcess = false;

		// Check if there are any records exists in the respective table to process
		try {
			acrRecord = this.acrProcessService.newRecordsToProcess(acrRecord, recordType);
			if ("sbb".equalsIgnoreCase(recordType)) {
				checkForAnyRecordsToProcess = !CollectionUtils.isEmpty(acrRecord.getSbbRecordList());
			} else if ("tnt".equalsIgnoreCase(recordType)) {
				checkForAnyRecordsToProcess = !CollectionUtils.isEmpty(acrRecord.getTransitTimeRecordList());
			} else if ("jwl".equalsIgnoreCase(recordType)) {
				checkForAnyRecordsToProcess = !CollectionUtils.isEmpty(acrRecord.getUpscJewelryRecordList());
			} else if ("ssh".equalsIgnoreCase(recordType)) {
				checkForAnyRecordsToProcess = !CollectionUtils.isEmpty(acrRecord.getSshRecordList());
			} else if ("fwr".equalsIgnoreCase(recordType)) {
				checkForAnyRecordsToProcess = !CollectionUtils.isEmpty(acrRecord.getFwrRecordList());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		logger.info(
				"CheckForAnyRecordsToProcess flag is " + checkForAnyRecordsToProcess + " for record Type" + recordType);
		if ((acrRecord != null) && (checkForAnyRecordsToProcess)) {
			synchronized (this) {
				try {

					boolean updatedAcr = false;
					boolean notificationSent = false;
					
					if(("ssh").equalsIgnoreCase(recordType)) {
						acrProcessDAO.archiveProcessedRecord(acrRecord, recordType);
						
						List<SshRecord> records = this.acrProcessDAO.newRecordsToProcessUpscSsh("Y");
						acrRecord.setSshRecordList(records);
					}
					
					// if there are any records populate the records to Excel file
					File excelTempFile = this.acrProcessService.populateRecordFromTableToExcelSheet(acrRecord,
							recordType);
					
					// Send notification email with the file as an attachment					
					if (excelTempFile != null) {
						notificationSent = this.emailService.sendNotificationEmail(excelTempFile, recordType);
					}

					// Update the processed records with the flag='Y'
					if (("sbb").equalsIgnoreCase(recordType) || ("tnt").equalsIgnoreCase(recordType)
							|| ("jwl").equalsIgnoreCase(recordType)) {
						if (notificationSent) {
							updatedAcr = this.acrProcessDAO.archiveProcessedRecord(acrRecord, recordType);
						}

						// Update request status for accountchange_request for process records
						if ((updatedAcr) && (notificationSent)) {
							this.acrProcessDAO.updateRequestStatus(acrRecord, recordType);
						}
					} else if (("fwr").equalsIgnoreCase(recordType)) {
						if (notificationSent) {
							updatedAcr = this.acrProcessDAO.archiveProcessedRecord(acrRecord, recordType);
						}

					}
					// Delete the temporary file created
					if (excelTempFile != null) {
						excelTempFile.delete();
					}
				} catch (Exception e) {
					e.printStackTrace();
					StringWriter sw = new StringWriter();
					e.printStackTrace(new PrintWriter(sw));
					logger.error(sw.toString());
				}
			}
		} else {
			logger.info("There are no records to process for " + recordType);
		}
	}
}
