package com.worldwide.service;

import java.io.File;
import java.io.FileInputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmailService {
	static final Logger logger = Logger.getLogger(EmailService.class);

	@Transactional
	public boolean sendNotificationEmail(File excelFolder, String recordType) {
		try {
			logger.info("Started sending notification email.");
			String date = new SimpleDateFormat("MM.dd.YYYY").format(new Date());
			String fromEmail = "UPSservices@wwex.com";
			String subject = "";
			String body = "";
			String toEmail = "";
			StringBuilder ccEmailSb = new StringBuilder();

			 String ccEmail = "sgurrala@wwex.com, cwring@wwex.com";
			 //String ccEmail = "sgurrala@wwex.com, sgurrala@wwex.com";
			ccEmailSb.append(ccEmail);
			if ((null != recordType) && ("sbb".equalsIgnoreCase(recordType))) {
				body = "Attached are the Scan Based Billing requests for the week of " + date;
				subject = "Scan Based Billing Additions " + date;
				toEmail = "wwecreditrequest@ups.com";
     			ccEmailSb.append(", UPSservices@wwex.com");
     			//toEmail = "sgurrala@wwex.com";
				//ccEmailSb.append(", sgurrala@wwex.com");
			} else if ((null != recordType) && ("jwl".equalsIgnoreCase(recordType))) {
				//toEmail = "UPSservices@wwex.com";
				toEmail = "JLongoria@ups.com, TRSStrategic@ups.com, JVidal@ups.com, tgunn@ups.com";
				ccEmailSb.append(", UPSservices@wwex.com");
				//toEmail = "sgurrala@wwex.com";
				body = "Attached are the UPS Capital Policy Additions - Jewelers Edge for the week of " + date;
				subject = "UPS Capital Policy Additions - Jewelers Edge " + date;
			} else if ((null != recordType) && ("tnt".equalsIgnoreCase(recordType))) {
				//toEmail = "UPSservices@wwex.com";
				toEmail = "JLongoria@ups.com, TRSStrategic@ups.com, JVidal@ups.com, tgunn@ups.com";
				ccEmailSb.append(", UPSservices@wwex.com");
				//toEmail = "sgurrala@wwex.com";
				body = "Attached are the UPS Capital Policy Additions - Time In Transit for the week of " + date;
				subject = "UPS Capital Policy Additions - Time In Transit " + date;
			} else if ((null != recordType) && ("ssh".equalsIgnoreCase(recordType))) {
				toEmail = "kmeyer@ups.com";
				ccEmailSb.append(", UPSservices@wwex.com");
			//	toEmail = "aprange@wwex.com, hollarzabal@wwex.com";
				body = "Attached are the Seasonal Shippers Requests for this week.";
				subject = "Seasonal Shippers Requests for " + date;
			} else if ((null != recordType) && ("fwr".equalsIgnoreCase(recordType))) {
				toEmail = "kmeyer@ups.com";
				ccEmailSb.append(", UPSservices@wwex.com");
				//toEmail = "aprange@wwex.com, hollarzabal@wwex.com";
				body = "Attached are the QVD Feed Requests for this week.";
				subject = "QVD Feed Requests for " + date;
			}

			Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@//scan01-927069.wwex.com:1521/WWEX", "wwex",
					"sp33dsh1p");
			CallableStatement procedure = connection
					.prepareCall("{ call WEXNET.PR_SEND_EMAIL_2(?,?,?,?,?,?,?,?,?,?,?) }");

			FileInputStream inputStream = new FileInputStream(excelFolder);

			procedure.setInt(1, 44);
			procedure.setString(2, toEmail);
			procedure.setString(3, subject);
			procedure.setString(4, body);
			procedure.setString(5, fromEmail);
			procedure.setString(6, null);
			procedure.setString(7, null);
			procedure.setString(8, ccEmailSb.toString());
			procedure.setString(9, null);
			procedure.setString(10, excelFolder.getName());
			procedure.setBlob(11, inputStream);
			int executeUpdate = procedure.executeUpdate();

			logger.info("Email sent to address: toEmail: " + toEmail + "ccEmail: " + ccEmailSb + " excelFolder name "
					+ excelFolder + " executeUpdate value " + executeUpdate);
			inputStream.close();

			procedure.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();

			logger.error("There was a problem sending the email.");

		} finally {
		}
		return true;
	}
}
