package com.worldwide.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class UtilService
{
  static final Logger logger = Logger.getLogger(UtilService.class);
  public static String environment = "";
  private Properties properties;
  private Connection connection;
  
  @PostConstruct
  private void initData()
  {
    try
    {
      InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties");
      this.properties = new Properties();
      this.properties.load(input);
      input.close();
      if (this.properties != null) {
        logger.info("Properties file successfully loaded.");
      }
      environment = environment.toUpperCase();
      logger.info("Environment parameter is: " + environment);
      
      int env = environment.equals("PROD") ? 3 : environment.equals("UAT") ? 2 : environment.equals("DEV") ? 1 : 0;
      String url, dbName, driver, username, password;
      switch (env)
      {
      case 1: 
        logger.info("Connection to " + environment + " database in progress...");
        url = this.properties.getProperty("worldwide.dev.database.url");
        dbName = this.properties.getProperty("worldwide.dev.database.database");
        driver = this.properties.getProperty("worldwide.dev.database.driver");
        username = this.properties.getProperty("worldwide.dev.database.username");
        password = this.properties.getProperty("worldwide.dev.database.password");
        Class.forName(driver).newInstance();
        this.connection = DriverManager.getConnection(url + "/" + dbName, username, password);
        break;
      case 2: 
        logger.info("Connection to " + environment + " database in progress...");
        url = this.properties.getProperty("worldwide.uat.database.url");
        dbName = this.properties.getProperty("worldwide.uat.database.database");
        driver = this.properties.getProperty("worldwide.uat.database.driver");
        username = this.properties.getProperty("worldwide.uat.database.username");
        password = this.properties.getProperty("worldwide.uat.database.password");
        Class.forName(driver).newInstance();
        this.connection = DriverManager.getConnection(url + "/" + dbName, username, password);
        break;
      case 3: 
        logger.info("Connection to " + environment + " database in progress...");
        url = this.properties.getProperty("worldwide.prod.database.url");
        dbName = this.properties.getProperty("worldwide.prod.database.database");
        driver = this.properties.getProperty("worldwide.prod.database.driver");
        username = this.properties.getProperty("worldwide.prod.database.username");
        password = this.properties.getProperty("worldwide.prod.database.password");
        Class.forName(driver).newInstance();
        this.connection = DriverManager.getConnection(url + "/" + dbName, username, password);
        break;
      default: 
        environment = this.properties.getProperty("worldwide.process.environment.default");
        logger.info("Environment parameter is not recognized. Default environment will be set to: " + environment);
        
        logger.info("Connection to " + environment + " database in progress...");
        url = this.properties.getProperty("worldwide.dev.database.url");
        dbName = this.properties.getProperty("worldwide.dev.database.database");
        driver = this.properties.getProperty("worldwide.dev.database.driver");
        username = this.properties.getProperty("worldwide.dev.database.username");
        password = this.properties.getProperty("worldwide.dev.database.password");
        Class.forName(driver).newInstance();
        this.connection = DriverManager.getConnection(url + "/" + dbName, username, password);
      }
      if ((this.connection != null) && (!this.connection.isClosed())) {
        logger.info(environment + " Database connection established.");
      }
    }
    catch (IOException e)
    {
      logger.error("Error trying to loading application.properties file.");
      e.printStackTrace();
    }
    catch (SQLException e)
    {
      logger.error("Error trying to access the Database.");
      e.printStackTrace();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  public Connection getDBConnection()
  {
    try
    {
      if (this.connection != null) {
        return this.connection;
      }
      initData();
      return this.connection;
    }
    catch (Exception e)
    {
      logger.error("There was an error trying to connect to the database.");
      e.printStackTrace();
    }
    return this.connection;
  }
}
