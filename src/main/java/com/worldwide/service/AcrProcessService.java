package com.worldwide.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.worldwide.dao.AcrProcessDAO;
import com.worldwide.model.AcrRecord;
import com.worldwide.model.FwrRecord;
import com.worldwide.model.SbbRecord;
import com.worldwide.model.SshRecord;
import com.worldwide.model.UpscJewelryRecord;
import com.worldwide.model.UpscTransitTimeRecord;

@Service
public class AcrProcessService {
	static final Logger logger = Logger.getLogger(AcrProcessService.class);
	@Autowired
	private AcrProcessDAO acrProcessDAO;

	public File populateSbbRecordFromTableToExcelSheet(List<SbbRecord> records) {
		logger.info("Started populating SBB records from table to Excel ");
		String date = new SimpleDateFormat("MM.dd.YYYY").format(new Date());
		FileInputStream file = null;
		File templateFile = new File("Template/ScanBasedBilling.xls");
		File genFile = null;
		try {
			FileUtils.cleanDirectory(new File("temp/SBB"));
			genFile = new File("temp/SBB/ScanBasedBilling_" + date.toString() + ".xls");

			FileUtils.copyFile(templateFile, genFile);
			file = new FileInputStream(genFile);
			HSSFWorkbook book = new HSSFWorkbook(file);
			HSSFSheet sheet0 = book.getSheetAt(0);
			int i = 2;
			for (SbbRecord rec : records) {
				Cell cell3 = sheet0.getRow(i).createCell(0);
				Cell cell4 = sheet0.getRow(i).createCell(1);
				Cell cell5 = sheet0.getRow(i).createCell(2);

				cell3.setCellValue(rec.getCompanyName());
				cell4.setCellValue(rec.getUpsAccountNo());
				cell5.setCellValue(rec.getAction());

				i++;
				sheet0.createRow(i);
			}
			FileOutputStream fileOut = new FileOutputStream(genFile);
			book.write(fileOut);

			book.close();
			fileOut.flush();
			fileOut.close();
			file.close();
			genFile.setExecutable(false);
			logger.info("Completed populating SBB records to file " + genFile.getName());
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			genFile = null;
		}
		return genFile;
	}

	public File populateUpscTntRecordFromTableToExcelSheet(List<UpscTransitTimeRecord> records) {
		logger.info("Started populating UPSC Tnt records from table to Excel ");
		String date = new SimpleDateFormat("MM.dd.YYYY").format(new Date());
		String dateRequested = new SimpleDateFormat("MM-dd-YYYY").format(new Date());
		FileInputStream file = null;
		File templateFile = new File("Template/UPSC-TNT.xls");
		File genFile = null;
		try {
			FileUtils.cleanDirectory(new File("temp/UpscTnt"));
			genFile = new File("temp/UpscTnt/UPS Capital Policy Additions - Time In Transit " + date.toString() + ".xls");

			FileUtils.copyFile(templateFile, genFile);
			file = new FileInputStream(genFile);
			HSSFWorkbook book = new HSSFWorkbook(file);
			HSSFSheet sheet0 = book.getSheetAt(0);
			int i = 2;
			for (UpscTransitTimeRecord rec : records) {
				sheet0.createRow(i);
				Cell cell0 = sheet0.getRow(i).createCell(0);
				Cell cell1 = sheet0.getRow(i).createCell(1);
				Cell cell2 = sheet0.getRow(i).createCell(2);
				Cell cell3 = sheet0.getRow(i).createCell(3);
				Cell cell4 = sheet0.getRow(i).createCell(4);
				Cell cell5 = sheet0.getRow(i).createCell(5);
				Cell cell6 = sheet0.getRow(i).createCell(6);
				Cell cell7 = sheet0.getRow(i).createCell(7);
				Cell cell8 = sheet0.getRow(i).createCell(8);

				cell0.setCellValue(dateRequested);
				cell1.setCellValue(rec.getCustomerName());
				cell2.setCellValue(rec.getCustomerNo());
				cell3.setCellValue(rec.getUpsNo());
				cell4.setCellValue(rec.getFranName());
				cell5.setCellValue(rec.getCommShip());
				cell6.setCellValue(rec.getDunsNum());
				cell7.setCellValue(rec.getEstwklyinsVal());
				cell8.setCellValue(rec.getRevTier());
				i++;
			}
			FileOutputStream fileOut = new FileOutputStream(genFile);

			book.write(fileOut);
			book.close();
			fileOut.flush();
			fileOut.close();
			file.close();
			genFile.setExecutable(false);
			logger.info("Completed populating UPSC Tnt records from table to Excel ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
		}
		return genFile;
	}

	private File populateUpscFwrRecordFromTableToExcelSheet(List<FwrRecord> fwrRecordList) {

		logger.info("Started populating UPSC Focus Web Request records from table to Excel ");
		String date = new SimpleDateFormat("MM.dd.YYYY").format(new Date());
		FileInputStream file = null;
		File templateFile = new File("Template/UPSC-FWR.xls");
		File genFile = null;
		try {
			FileUtils.cleanDirectory(new File("temp/FWR"));
			genFile = new File("temp/FWR/QVD Feed Requests for " + date.toString() + ".xls");

			FileUtils.copyFile(templateFile, genFile);
			file = new FileInputStream(genFile);
			HSSFWorkbook book = new HSSFWorkbook(file);
			HSSFSheet sheet0 = book.getSheetAt(0);
			int i = 1;
			for (FwrRecord rec : fwrRecordList) {
				Cell cell0 = sheet0.getRow(i).createCell(0);
				Cell cell1 = sheet0.getRow(i).createCell(1);

				cell0.setCellValue(rec.getUpsAccountNum());
				cell1.setCellValue(rec.getAction());
			

				i++;
				sheet0.createRow(i);
			}
			FileOutputStream fileOut = new FileOutputStream(genFile);

			book.write(fileOut);
			book.close();
			fileOut.flush();
			fileOut.close();
			file.close();
			genFile.setExecutable(false);
			logger.info("Completed populating UPSC FWR records from table to Excel ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
		}
		return genFile;
	
	}

	private File populateUpscSshRecordFromTableToExcelSheet(List<SshRecord> records) {

		logger.info("Started populating UPSC Seasonal shippers records from table to Excel ");
		String date = new SimpleDateFormat("MM.dd.YYYY").format(new Date());
		FileInputStream file = null;
		File templateFile = new File("Template/UPSC-SSH.xls");
		File genFile = null;
		try {
			FileUtils.cleanDirectory(new File("temp/SSH"));
			genFile = new File("temp/SSH/Seasonal Shippers Requests for " + date.toString() + ".xls");

			FileUtils.copyFile(templateFile, genFile);
			file = new FileInputStream(genFile);
			HSSFWorkbook book = new HSSFWorkbook(file);
			HSSFSheet sheet0 = book.getSheetAt(0);
			int i = 1;
			for (SshRecord rec : records) {
				Cell cell0 = sheet0.getRow(i).createCell(0);
				Cell cell1 = sheet0.getRow(i).createCell(1);
				Cell cell2 = sheet0.getRow(i).createCell(2);

				cell0.setCellValue(rec.getCompanyName());
				cell1.setCellValue(rec.getCustomerNum());
				cell2.setCellValue(rec.getUpsAccountNum());
			

				i++;
				sheet0.createRow(i);
			}
			FileOutputStream fileOut = new FileOutputStream(genFile);

			book.write(fileOut);
			book.close();
			fileOut.flush();
			fileOut.close();
			file.close();
			genFile.setExecutable(false);
			logger.info("Completed populating UPSC SSH records from table to Excel ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
		}
		return genFile;
	
	}
	
	public File populateUpscJwlRecordFromTableToExcelSheet(List<UpscJewelryRecord> records) {
		logger.info("Started populating UPSC JWL records from table to Excel ");
		String date = new SimpleDateFormat("MM.dd.YYYY").format(new Date());
		String dateRequested = new SimpleDateFormat("MM-dd-YYYY").format(new Date());
		FileInputStream file = null;
		File templateFile = new File("Template/UPSC-JWL.xls");
		File genFile = null;
		try {
			FileUtils.cleanDirectory(new File("temp/UpscJwl"));
			genFile = new File("temp/UpscJwl/UPS Capital Policy Additions - Jewelers Edge " + date.toString() + ".xls");

			FileUtils.copyFile(templateFile, genFile);
			file = new FileInputStream(genFile);
			HSSFWorkbook book = new HSSFWorkbook(file);
			HSSFSheet sheet0 = book.getSheetAt(0);
			int i = 2;
			for (UpscJewelryRecord rec : records) {
				Cell cell0 = sheet0.getRow(i).createCell(0);
				Cell cell1 = sheet0.getRow(i).createCell(1);
				Cell cell2 = sheet0.getRow(i).createCell(2);
				Cell cell3 = sheet0.getRow(i).createCell(3);
				Cell cell4 = sheet0.getRow(i).createCell(4);
				Cell cell5 = sheet0.getRow(i).createCell(5);
				Cell cell6 = sheet0.getRow(i).createCell(6);
				Cell cell7 = sheet0.getRow(i).createCell(7);

				cell0.setCellValue(dateRequested);
				cell1.setCellValue(rec.getCustomerName());
				cell2.setCellValue(rec.getCustomerNo());
				cell3.setCellValue(rec.getUpsNo());
				cell4.setCellValue(rec.getFranName());
				cell5.setCellValue(rec.getDunsNum());
				cell6.setCellValue(rec.getEstwklyinsVal());
				cell7.setCellValue(rec.getRevTier());

				i++;
				sheet0.createRow(i);
			}
			FileOutputStream fileOut = new FileOutputStream(genFile);

			book.write(fileOut);
			book.close();
			fileOut.flush();
			fileOut.close();
			file.close();
			genFile.setExecutable(false);
			logger.info("Completed populating UPSC Jwl records from table to Excel ");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
		}
		return genFile;
	}

	public File populateRecordFromTableToExcelSheet(AcrRecord acrRecord, String recordType) {
		if ((!StringUtils.isEmpty(recordType)) && ("sbb".equalsIgnoreCase(recordType))) {
			return populateSbbRecordFromTableToExcelSheet(acrRecord.getSbbRecordList());
		}
		if ((!StringUtils.isEmpty(recordType)) && ("tnt".equalsIgnoreCase(recordType))) {
			return populateUpscTntRecordFromTableToExcelSheet(acrRecord.getTransitTimeRecordList());
		}
		if ((!StringUtils.isEmpty(recordType)) && ("jwl".equalsIgnoreCase(recordType))) {
			return populateUpscJwlRecordFromTableToExcelSheet(acrRecord.getUpscJewelryRecordList());
		}
		if ((!StringUtils.isEmpty(recordType)) && ("ssh".equalsIgnoreCase(recordType))) {
			return populateUpscSshRecordFromTableToExcelSheet(acrRecord.getSshRecordList());
		}
		if ((!StringUtils.isEmpty(recordType)) && ("fwr".equalsIgnoreCase(recordType))) {
			return populateUpscFwrRecordFromTableToExcelSheet(acrRecord.getFwrRecordList());
		}
		return null;
	}

	public AcrRecord newRecordsToProcess(AcrRecord acrRecord, String recordType) {
		try {
			logger.info("Fetching new records for " + recordType);
			if ((!StringUtils.isEmpty(recordType)) && ("sbb".equalsIgnoreCase(recordType))) {
				List<SbbRecord> records = this.acrProcessDAO.newRecordsToProcessSbb();
				acrRecord.setSbbRecordList(records);
			} else if ((!StringUtils.isEmpty(recordType)) && ("tnt".equalsIgnoreCase(recordType))) {
				List<UpscTransitTimeRecord> records = this.acrProcessDAO.newRecordsToProcessUpscTnt();
				acrRecord.setTransitTimeRecordList(records);
			} else if ((!StringUtils.isEmpty(recordType)) && ("jwl".equalsIgnoreCase(recordType))) {
				List<UpscJewelryRecord> records = this.acrProcessDAO.newRecordsToProcessUpscJwl();
				acrRecord.setUpscJewelryRecordList(records);
			} else if ((!StringUtils.isEmpty(recordType)) && ("ssh".equalsIgnoreCase(recordType))) {
				List<SshRecord> records = this.acrProcessDAO.newRecordsToProcessUpscSsh("N");
				acrRecord.setSshRecordList(records);
			} else if ((!StringUtils.isEmpty(recordType)) && ("fwr".equalsIgnoreCase(recordType))) {
				List<FwrRecord> records = this.acrProcessDAO.newRecordsToProcessUpscFwr();
				acrRecord.setFwrRecordList(records);
			}
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return acrRecord;
		}
		logger.info("Completed fetching new records for " + recordType);
		return acrRecord;
	}
}
