package com.worldwide.dao;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.worldwide.model.AcrRecord;
import com.worldwide.model.FwrRecord;
import com.worldwide.model.SbbRecord;
import com.worldwide.model.SshRecord;
import com.worldwide.model.UpscJewelryRecord;
import com.worldwide.model.UpscTransitTimeRecord;
import com.worldwide.service.UtilService;

@Component
public class AcrProcessDAO {
	static final Logger logger = Logger.getLogger(AcrProcessDAO.class);
	@Autowired
	private UtilService utilService;

	@Transactional
	public List<SbbRecord> newRecordsToProcessSbb() {
		List<SbbRecord> records = new ArrayList<SbbRecord>();
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			String query = "select ID, REQUESTORFULLNAME, REQUESTOREMAIL, COMPANYNAME, UPSACCOUNTNO, ACR_ID, ACTION from WEXNET.sbb_acr where (PROCESSFLAG!='Y' OR PROCESSFLAG is NULL)";
			statement = this.utilService.getDBConnection().prepareStatement(query);
			result = statement.executeQuery();
			SbbRecord record;
			while (result.next()) {
				record = new SbbRecord();
				record.setRequestorId(result.getString("ID"));
				record.setRequestorFullName(result.getString("REQUESTORFULLNAME"));
				record.setRequestorEmail(result.getString("REQUESTOREMAIL"));
				record.setCompanyName(result.getString("COMPANYNAME"));
				record.setUpsAccountNo(result.getString("UPSACCOUNTNO"));
				record.setAcrId(result.getString("ACR_ID"));
				record.setAction(result.getString("ACTION"));
				records.add(record);
			}
			return records;
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return records;
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	public List<FwrRecord> newRecordsToProcessUpscFwr() {
		List<FwrRecord> records = new ArrayList<FwrRecord>();
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			String query = "select ID, REQUESTORNAME, REQUESTOREMAIL, COMPANYNAME, UPSACCOUNTNO, ACTION, CUSTOMERNO from WEXNET.fwr_Acr where PROCESSFLAG='N'";
			statement = this.utilService.getDBConnection().prepareStatement(query);
			result = statement.executeQuery();
			FwrRecord record;
			while (result.next()) {
				record = new FwrRecord();
				record.setRequestorId(result.getString("ID"));
				record.setRequestorEmail(result.getString("REQUESTOREMAIL"));
				record.setCompanyName(result.getString("COMPANYNAME"));
				record.setCustomerNum(result.getString("CUSTOMERNO"));
				record.setUpsAccountNum(result.getString("UPSACCOUNTNO"));
				record.setRequestorName(result.getString("REQUESTORNAME"));
				record.setAction(result.getString("ACTION"));
				records.add(record);
			}
			return records;
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return records;
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	
	public List<SshRecord> newRecordsToProcessUpscSsh(String flag) {
		List<SshRecord> records = new ArrayList<SshRecord>();
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			String query = "select ID, COMPANYNAME, CUSTOMERNO, UPSACCOUNTNO, PROCESSFLAG, REQUESTORNAME, REQUESTOREMAIL from WEXNET.ssr_Acr where PROCESSFLAG= '"+flag+"'";
			statement = this.utilService.getDBConnection().prepareStatement(query);
			result = statement.executeQuery();
			SshRecord record;
			while (result.next()) {
				record = new SshRecord();
				record.setRequestorId(result.getString("ID"));
				record.setRequestorEmail(result.getString("REQUESTOREMAIL"));
				record.setCompanyName(result.getString("COMPANYNAME"));
				record.setCustomerNum(result.getString("CUSTOMERNO"));
				record.setUpsAccountNum(result.getString("UPSACCOUNTNO"));
				record.setRequestorName(result.getString("REQUESTORNAME"));
				records.add(record);
			}
			return records;
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return records;
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}
	
	@Transactional
	public List<UpscTransitTimeRecord> newRecordsToProcessUpscTnt() {
		List<UpscTransitTimeRecord> records = new ArrayList<UpscTransitTimeRecord>();
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			String query = "select ID, REQUESTER_NAME,REQUESTER_EMAIL,DATE_REQUESTED,CUSTOMERNAME,UPSNO,FRAN_NAME,ESTWKLYINSVAL, REVTIER, DUNS, COMMSHIP, CUSTOMERNO, ACR_ID       from WEXNET.tnt_acr where (PROCESSFLAG!='Y' OR PROCESSFLAG is NULL)";
			statement = this.utilService.getDBConnection().prepareStatement(query);
			result = statement.executeQuery();
			UpscTransitTimeRecord record;
			while (result.next()) {
				record = new UpscTransitTimeRecord();
				record.setRequestorId(result.getString("ID"));
				record.setRequestorFullName(result.getString("REQUESTER_NAME"));
				record.setRequestorEmail(result.getString("REQUESTER_EMAIL"));
				record.setDateRequested(result.getString("DATE_REQUESTED"));
				record.setCustomerName(result.getString("CUSTOMERNAME"));
				record.setCustomerNo(result.getString("CUSTOMERNO"));
				record.setUpsNo(result.getString("UPSNO"));
				record.setFranName(result.getString("FRAN_NAME"));
				record.setEstwklyinsVal(result.getString("ESTWKLYINSVAL"));
				record.setRevTier(result.getString("REVTIER"));
				record.setDunsNum(result.getString("DUNS"));
				record.setCommShip(result.getString("COMMSHIP"));
				record.setAcrId(result.getString("ACR_ID"));
				records.add(record);
			}
			return records;
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return records;
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	@Transactional
	public List<UpscJewelryRecord> newRecordsToProcessUpscJwl() {
		List<UpscJewelryRecord> records = new ArrayList<UpscJewelryRecord>();
		PreparedStatement statement = null;
		ResultSet result = null;
		try {
			String query = "select ID, REQUESTER_NAME, REQUESTER_EMAIL, DATE_REQUESTED, CUSTOMERNAME, CUSTOMERNO,UPSNO, FRAN_NAME, ESTWKLYINSVAL, REVTIER, DUNS, ACR_ID   from WEXNET.jpa_acr where (PROCESSFLAG!='Y' OR PROCESSFLAG is NULL)";
			statement = this.utilService.getDBConnection().prepareStatement(query);
			result = statement.executeQuery();
			UpscJewelryRecord record;
			while (result.next()) {
				record = new UpscJewelryRecord();
				record.setRequestorId(result.getString("ID"));
				record.setRequestorFullName(result.getString("REQUESTER_NAME"));
				record.setRequestorEmail(result.getString("REQUESTER_EMAIL"));
				record.setDateRequested(result.getString("DATE_REQUESTED"));
				record.setCustomerName(result.getString("CUSTOMERNAME"));
				record.setCustomerNo(result.getString("CUSTOMERNO"));
				record.setUpsNo(result.getString("UPSNO"));
				record.setFranName(result.getString("FRAN_NAME"));
				record.setEstwklyinsVal(result.getString("ESTWKLYINSVAL"));
				record.setRevTier(result.getString("REVTIER"));
				record.setDunsNum(result.getString("DUNS"));
				record.setAcrId(result.getString("ACR_ID"));
				records.add(record);
			}
			return records;
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return records;
		} finally {
			try {
				if (result != null) {
					result.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	public boolean archiveProcessedRecord(AcrRecord acrRecord, String recordType) {
		PreparedStatement statement = null;
		String query = " ";
		StringBuilder queryBuilder = new StringBuilder();
		String dateRequested = new SimpleDateFormat("MM/dd/YYYY").format(new Date());
		
		try {
			if (("sbb".equalsIgnoreCase(recordType)) && (!CollectionUtils.isEmpty(acrRecord.getSbbRecordList()))) {
				logger.info("Updating processed records with ProcessFLag='Y' for recordType " + recordType);
				List<SbbRecord> records = acrRecord.getSbbRecordList();
				query = "update wexnet.sbb_acr set PROCESSFLAG='Y' where ID in (";
				queryBuilder.append(query);
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((SbbRecord) records.get(i)).getRequestorId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			} else if (("tnt".equalsIgnoreCase(recordType))
					&& (!CollectionUtils.isEmpty(acrRecord.getTransitTimeRecordList()))) {
				logger.info("Updating processed records with ProcessFLag='Y' for recordType " + recordType);
				List<UpscTransitTimeRecord> records = acrRecord.getTransitTimeRecordList();
				query = "update wexnet.tnt_acr set PROCESSFLAG='Y' , DATE_REQUESTED = to_date('"+dateRequested+"', 'MM/dd/YYYY') where ID in (";
				queryBuilder.append(query);
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((UpscTransitTimeRecord) records.get(i)).getRequestorId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			} else if (("jwl".equalsIgnoreCase(recordType))
					&& (!CollectionUtils.isEmpty(acrRecord.getUpscJewelryRecordList()))) {
				logger.info("Updating processed records with ProcessFLag='Y' for recordType " + recordType);
				List<UpscJewelryRecord> records = acrRecord.getUpscJewelryRecordList();
				query = "update wexnet.jpa_acr set PROCESSFLAG='Y' , DATE_REQUESTED = to_date('"+dateRequested+"', 'MM/dd/YYYY') where ID in (";
				queryBuilder.append(query);
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((UpscJewelryRecord) records.get(i)).getRequestorId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			}
			else if (("fwr".equalsIgnoreCase(recordType))
					&& (!CollectionUtils.isEmpty(acrRecord.getFwrRecordList()))) {
				logger.info("Updating processed records with ProcessFLag='Y' for recordType " + recordType);
				List<FwrRecord> records = acrRecord.getFwrRecordList();
				query = "update wexnet.fwr_acr set PROCESSFLAG='Y', SUBMIT_DATE= to_date('"+dateRequested+"', 'MM/dd/YYYY') where ID in (";
				queryBuilder.append(query);
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((FwrRecord) records.get(i)).getRequestorId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			}
			else if (("ssh".equalsIgnoreCase(recordType))
					&& (!CollectionUtils.isEmpty(acrRecord.getSshRecordList()))) {
				logger.info("Updating processed records with ProcessFLag='Y' for recordType " + recordType);
				List<SshRecord> records = acrRecord.getSshRecordList();
				query = "update wexnet.ssr_acr set PROCESSFLAG='Y', SUBMIT_DATE= to_date('"+dateRequested+"', 'MM/dd/YYYY') where ID in (";
				queryBuilder.append(query);
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((SshRecord) records.get(i)).getRequestorId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			}
			queryBuilder.append(")");
			logger.info(queryBuilder.toString());
			statement = this.utilService.getDBConnection().prepareStatement(queryBuilder.toString());

			statement.executeQuery().close();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return false;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				logger.error(sw.toString());
			}
		}
	}

	public boolean updateRequestStatus(AcrRecord acrRecord, String recordType) {
		PreparedStatement statement = null;
		String query = " ";
		StringBuilder queryBuilder = new StringBuilder();
		logger.info("Started updating table accountchange_requests.");
		try {
			String date = new SimpleDateFormat("MM/dd/YYYY").format(new Date());
			query = "update wexnet.accountchange_requests set REQUESTSTATUS='1', SUBMITTEDON =to_date('"+date+"', 'MM/dd/YYYY') where ID in (";
			queryBuilder.append(query);
			if ("sbb".equalsIgnoreCase(recordType)) {
				List<SbbRecord> records = acrRecord.getSbbRecordList();
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((SbbRecord) records.get(i)).getAcrId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			} else if ("tnt".equalsIgnoreCase(recordType)) {
				List<UpscTransitTimeRecord> records = acrRecord.getTransitTimeRecordList();
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((UpscTransitTimeRecord) records.get(i)).getAcrId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			} else if ("jwl".equalsIgnoreCase(recordType)) {
				List<UpscJewelryRecord> records = acrRecord.getUpscJewelryRecordList();
				for (int i = 0; i < records.size(); i++) {
					queryBuilder.append(((UpscJewelryRecord) records.get(i)).getAcrId());
					if (i != records.size() - 1) {
						queryBuilder.append(",");
					}
				}
			}
			queryBuilder.append(")");
			statement = this.utilService.getDBConnection().prepareStatement(queryBuilder.toString());
			logger.info(queryBuilder.toString());
			statement.executeQuery().close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			logger.error(sw.toString());
			return false;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				logger.error(sw.toString());
			}
		}
	}
}
