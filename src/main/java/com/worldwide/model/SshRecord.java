package com.worldwide.model;

public class SshRecord {

	private String requestorId;
	private String companyName;
	private String customerNum;
	private String upsAccountNum;
	private String processFlag;
	private String acrId;
	private String requestorName;
	private String requestorEmail;
	
	public String getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCustomerNum() {
		return customerNum;
	}
	public void setCustomerNum(String customerNum) {
		this.customerNum = customerNum;
	}
	public String getUpsAccountNum() {
		return upsAccountNum;
	}
	public void setUpsAccountNum(String upsAccountNum) {
		this.upsAccountNum = upsAccountNum;
	}
	public String getProcessFlag() {
		return processFlag;
	}
	public void setProcessFlag(String processFlag) {
		this.processFlag = processFlag;
	}
	public String getAcrId() {
		return acrId;
	}
	public void setAcrId(String acrId) {
		this.acrId = acrId;
	}
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public String getRequestorEmail() {
		return requestorEmail;
	}
	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}	
	
}
