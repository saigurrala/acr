package com.worldwide.model;

import java.util.List;

public class AcrRecord {
	public List<UpscJewelryRecord> upscJewelryRecordList;
	public List<UpscTransitTimeRecord> transitTimeRecordList;
	public List<SbbRecord> sbbRecordList;
	public List<SshRecord> sshRecordList;
	public List<FwrRecord> fwrRecordList;

	public List<UpscJewelryRecord> getUpscJewelryRecordList() {
		return this.upscJewelryRecordList;
	}

	public void setUpscJewelryRecordList(List<UpscJewelryRecord> upscJewelryRecordList) {
		this.upscJewelryRecordList = upscJewelryRecordList;
	}

	public List<UpscTransitTimeRecord> getTransitTimeRecordList() {
		return this.transitTimeRecordList;
	}

	public void setTransitTimeRecordList(List<UpscTransitTimeRecord> transitTimeRecordList) {
		this.transitTimeRecordList = transitTimeRecordList;
	}

	public List<SbbRecord> getSbbRecordList() {
		return this.sbbRecordList;
	}

	public void setSbbRecordList(List<SbbRecord> sbbRecordList) {
		this.sbbRecordList = sbbRecordList;
	}

	public List<SshRecord> getSshRecordList() {
		return sshRecordList;
	}

	public void setSshRecordList(List<SshRecord> sshRecordList) {
		this.sshRecordList = sshRecordList;
	}

	public List<FwrRecord> getFwrRecordList() {
		return fwrRecordList;
	}

	public void setFwrRecordList(List<FwrRecord> fwrRecordList) {
		this.fwrRecordList = fwrRecordList;
	}

	
}
