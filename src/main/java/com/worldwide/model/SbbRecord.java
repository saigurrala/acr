package com.worldwide.model;

import java.io.Serializable;

public class SbbRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	private String requestorId;
	private String requestorFullName;
	private String requestorEmail;
	private String companyName;
	private String upsAccountNo;
	private String acrId;
	private String action;

	public String getRequestorId() {
		return this.requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorFullName() {
		return this.requestorFullName;
	}

	public void setRequestorFullName(String requestorFullName) {
		this.requestorFullName = requestorFullName;
	}

	public String getRequestorEmail() {
		return this.requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUpsAccountNo() {
		return this.upsAccountNo;
	}

	public void setUpsAccountNo(String upsAccountNo) {
		this.upsAccountNo = upsAccountNo;
	}

	public String getAcrId() {
		return this.acrId;
	}

	public void setAcrId(String acrId) {
		this.acrId = acrId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
