package com.worldwide.model;

import java.io.Serializable;

public class UpscJewelryRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	private String requestorId;
	private String requestorFullName;
	private String requestorEmail;
	private String dateRequested;
	private String customerName;
	private String customerNo;
	private String upsNo;
	private String franName;
	private String estwklyinsVal;
	private String revTier;
	private String status;
	private String dunsNum;
	private String acrId;

	public String getRequestorId() {
		return this.requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorFullName() {
		return this.requestorFullName;
	}

	public void setRequestorFullName(String requestorFullName) {
		this.requestorFullName = requestorFullName;
	}

	public String getRequestorEmail() {
		return this.requestorEmail;
	}

	public void setRequestorEmail(String requestorEmail) {
		this.requestorEmail = requestorEmail;
	}

	public String getDateRequested() {
		return this.dateRequested;
	}

	public void setDateRequested(String dateRequested) {
		this.dateRequested = dateRequested;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getUpsNo() {
		return this.upsNo;
	}

	public void setUpsNo(String upsNo) {
		this.upsNo = upsNo;
	}

	public String getFranName() {
		return this.franName;
	}

	public void setFranName(String franName) {
		this.franName = franName;
	}

	public String getEstwklyinsVal() {
		return this.estwklyinsVal;
	}

	public void setEstwklyinsVal(String estwklyinsVal) {
		this.estwklyinsVal = estwklyinsVal;
	}

	public String getRevTier() {
		return this.revTier;
	}

	public void setRevTier(String revTier) {
		this.revTier = revTier;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDunsNum() {
		return this.dunsNum;
	}

	public void setDunsNum(String dunsNum) {
		this.dunsNum = dunsNum;
	}

	public String getAcrId() {
		return this.acrId;
	}

	public void setAcrId(String acrId) {
		this.acrId = acrId;
	}
}
