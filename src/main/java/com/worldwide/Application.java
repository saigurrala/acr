package com.worldwide;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.worldwide.service.UtilService;

@SpringBootApplication
@EnableAsync
@Component
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.worldwide")
public class Application implements CommandLineRunner {

	final static Logger logger = Logger.getLogger(Application.class);

	public ApplicationContext context;

	public void setContext(ApplicationContext context) {
		this.context = context;
	}

	@Override
	public void run(String... args) throws Exception {
		context = new AnnotationConfigApplicationContext(AppConfig.class);
	}

	public static void main(String[] args) {
		if (args.length == 1) {
			UtilService.environment = args[0].toUpperCase();
			logger.info("Starting the application in environment : " + UtilService.environment);
		}
		SpringApplication.run(Application.class, args);
	}

}

@Configuration
@Component
@EnableAutoConfiguration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = "com.worldwide")
class AppConfig extends Application {

	@Scheduled(cron = "${sbb.scheduling.job.cron}", zone = "CST")
	public void processSbbAddition() {
		if (context != null) {
			logger.info("\r\n");
			logger.info("Started processing SBB Addition on " + new Date());
			AcrProcessMain process = (AcrProcessMain) context.getBean("acrProcessMain");
			process.processAcr("sbb");
			logger.info("Completed processing SBB Addition on " + new Date());
		}
	}

	@Scheduled(cron = "${tnt.scheduling.job.cron}", zone = "CST")
	public void processUpscTransitTime() {
		if (context != null) {
			logger.info("\r\n");
			logger.info("Started processing UPSC TransitTime on " + new Date());
			AcrProcessMain process = (AcrProcessMain) context.getBean("acrProcessMain");
			process.processAcr("tnt");
			logger.info("Completed processing UPSC TransitTime on " + new Date());
		}
	}

	@Scheduled(cron = "${jpa.scheduling.job.cron}", zone = "CST")
	public void processUpscJewellery() {
		if (context != null) {
			logger.info("\r\n");
			logger.info("Started processing UPSC Jewellery on " + new Date());
			AcrProcessMain process = (AcrProcessMain) context.getBean("acrProcessMain");
			process.processAcr("JWL");
			logger.info("Completed processing UPSC Jewellery on " + new Date());
		}
	}
	
	@Scheduled(cron = "${ssh.scheduling.job.cron}", zone = "CST")
	public void processSeasonalShippers() {
		if (context != null) {
			logger.info("\r\n");
			logger.info("Started processing Seasonal shippers on " + new Date());
			AcrProcessMain process = (AcrProcessMain) context.getBean("acrProcessMain");
			process.processAcr("SSH");
			logger.info("Completed processing UPSC Seasonal shippers on " + new Date());
		}
	}
	
	@Scheduled(cron = "${fwr.scheduling.job.cron}", zone = "CST")
	public void processFocusWebRequest() {
		if (context != null) {
			logger.info("\r\n");
			logger.info("Started processing Focus Web Request on " + new Date());
			AcrProcessMain process = (AcrProcessMain) context.getBean("acrProcessMain");
			process.processAcr("FWR");
			logger.info("Completed processing Focus Web Request on " + new Date());
		}
	}
}